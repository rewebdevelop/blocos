$(document).ready(function(){
  /** FAZ O TOGGLE DO MENU MOBILE **/
  $(document).on('click', '.toggle-menu-system', function(){
    $('body').toggleClass('menu-on-system');
  });

  /** INICIALIZA O WOW.JS **/
  new WOW().init();

  /** MASCARA DOS FORMULARIOS **/
  $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('#num').mask('00000000000');
  $('#cep').mask('00000-000');
  $('#telefone').mask('(00)0000-00009');

  /** MULTISELECT **/
  $('.step-4 .select').multiselect({
    buttonText: function(options, select){
      return options[0].innerHTML;
    }
  });
  $('.step-4 .select').each(function(a, b){
    var $this = $(this);
    var text = $this.data('text');
    $this.siblings('.btn-group').find('.multiselect').prepend('<span class="before-text">'+text+': </span>');
  });

  /** SLY SCROLL PARA O STEP 4 **/
  var $frame  = $('#basic');
	var $slidee = $frame.children('ul').eq(0);
	var $wrap   = $frame.parent();
	$frame.sly({
		horizontal: 1,
		itemNav: 'basic',
		activateOn: 'click',
		mouseDragging: 0,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBar: $('.step-4 .scrollbar'),
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
    scrollHijack: 0
	});

  /** MUDANÇA DE CATEGORIA DE BLOCOS **/
  var all = $('.list-blocos').html();
  $(document).on('change', '#block-type', function(){
    resetBlocks(this.value);
  });
  function resetBlocks(value){
    console.log(value);
    $('.list-blocos').html(all);
    $('.list-blocos li:not(.'+value+')').remove();
    $('.frame').sly('reload');
    drag();
  }

  /** EXIBE/OCULTA FOOTER STEP 4 **/
  $(document).on('click', '.toggle-4', function(evt){
    evt.preventDefault();
    $('body').toggleClass('no-footer');
  });
  $(document).on('click', '#add-more', function(evt){
    evt.preventDefault();
    $('body').removeClass('no-footer');
  });

  /// Envia o ID do bloco e pega seu HTML
  function getBloco(id, callback){
    $.ajax({
      url: 'http://localhost:9000/src/get_blocos.php', /// Mudar
      dataType: 'html',
      method: 'post',
      data: {id: id},
      success: function(data){
        callback(data);
      },
      error: function(){
        return 'Erro ao carregar o bloco!';
      }
    });
  }

  /// Função que faz o draggable e é chamada novamente cada vez que atualiza o tipo de bloco
  function drag(){
    $(".list-blocos li").draggable({
      containment: 'body',
      cursor: "move",
      connectToSortable: ".sortable",
      appendTo: 'body',
      scroll: true,
      helper: 'clone',
      revert: 'invalid'
    });
  }
  drag(); /// Chama a função pela primeira vez

  /// Permite os itens serem reposicionados
  var $sortable = $('.sortable');
  $sortable.disableSelection()
  $sortable.sortable({
    placeholder: "sortable-placeholder",
    handle: '.handler',
    helper: function(){
      return '<span class="sort-helper"><i class="fa fa-arrows"></i></span>';
    },
    items: 'li.sorting:not(.header, .footer)',
    containment: 'body',
    revert: 'invalid',
    start: function(e, ui){
      $(ui.helper).addClass('helper');
    },
    start: function(e, ui){
      ui.placeholder.height(ui.item.height());
    },
    stop: function(e, ui){
      /// Quando os itens são "soltos"/reposicionados
      $(ui.item).removeClass('helper');
      $(ui.item).css('width', '100%');
      if(!$(ui.item).hasClass('sorting')){
        $(ui.item).addClass('sorting');
        var id = $(ui.item).data('id');
        var html = getBloco(id, function(html){
          $(ui.item).html(html);
        });
        // Reconstroi os slider com slick (Após chamar o html)
        setTimeout(function(){
          slickRebuilds(); /// slick-rebuild.js
        },300);
        // Adiciona o handler após reconstruir o slider com o slick
        setTimeout(function(){
          $('<span></span>').addClass('handler').prependTo($(ui.item));
        },400);
      }
    }
  });

  /// Edição de bloco
  $(document).on('click', '.sortable li', function(evt){
    evt.preventDefault();
    var $edit = $('.edit-config');
    var $this = $(this);
    var blocoId = $this.data('itemid');
    $this.siblings('li').removeClass('active-click');
    $this.addClass('active-click');
    $edit.fadeIn();
    $edit.attr('data-bloco', blocoId);
  });
  /// Excluir bloco
  $(document).on('click', '#remove', function(){
    var $this = $(this);
    var id    = $('.edit-config').attr('data-bloco');
    var $item = $('.sortable li[data-itemid="'+id+'"]');
    $('.edit-config').attr('data-bloco', '');
    $item.remove();
  });
  /// Mover bloco para cima
  $(document).on('click', '#block-up', function(){
    var $this = $(this);
    var id    = $('.edit-config').attr('data-bloco');
    var $item = $('.sortable li[data-itemid="'+id+'"]');
    var $prev = $item.prev();
    if(!$item.hasClass('header') && !$item.hasClass('footer')){
      if($prev.hasClass('item')){
        if(!$prev.hasClass('footer') && !$prev.hasClass('header')){
          var $clone = $item.clone(true);
          $item.prev().before($clone);
          $item.remove();
        }
      }
    }
  });
  /// Mover bloco para baixo
  $(document).on('click', '#block-down', function(){
    var $this = $(this);
    var id    = $('.edit-config').attr('data-bloco');
    var $item = $('.sortable li[data-itemid="'+id+'"]');
    var $next = $item.next();
    if(!$item.hasClass('header') && !$item.hasClass('footer')){
      if($next.hasClass('item')){
        if(!$next.hasClass('footer') && !$next.hasClass('header')){
          var $clone = $item.clone(true);
          $next.after($clone);
          $item.remove();
        }
      }
    }
  });
  $(document).scroll(function(){
    $('.edit-config').fadeOut();
  });

  /// Adicionar bloco acima - mobile
  $(document).on('click', '.item .arrow', function(){
    var self = $(this);
    var $item = self.closest('.item');
    var id = $item.data('id');
    var html = getBloco(id, function(html){
      var clone = $item.clone(true);
      clone.html(html);
      if(self.hasClass('arrow-up')){
        clone.prependTo('.sortable');
      }else if(self.hasClass('arrow-down')){
        clone.appendTo('.sortable');
      }
    });
    // Reconstroi os slider com slick (Após chamar o html)
    setTimeout(function(){
      slickRebuilds(); /// slick-rebuild.js
    },300);
  });

  /// Finalizar o step 4
  $(document).on('click', '#finish-step-4', function(evt){
    evt.preventDefault();
    var blocksPositions = [];
    $sortable.children().each(function(pos, item){
      blocksPositions.push($(item).data('id'));
    });
    console.log(blocksPositions);
  });
});
