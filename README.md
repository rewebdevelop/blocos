# Blocos
### Um guia de instalação e uso do sistema de blocos

O sitema de blocos utiliza [Gulp.js](http://gulpjs.com/) para automatizar algumas tarefas de front. Para instalação e utilização do sistema, siga os seguintes passos:

---
No o CMD do Windows, navegue até a pasta onde você clonou o repositório **blocos**. Depois digite o seguinte comando:
``` npm intall ```
para instalar as dependências do Gulp.
---
Após finalizada a instalação, utilize o comando
``` gulp ```
para iniciar as tarefas.
----
Você provavelmente irá precisar rodar o PHP em sua máquina. Para isso, se você utiliza o xampp, use o comando:
``` C:\xampp\php\php.exe -S localhost:9000 ```
---
### Tarefas:
* **sass**: Todos os arquivos do caminho src/sass são compilados na pasta src/stylesheets quando essa tarefa é chamada.    
* **watch**: Assiste modificações em arquivos .scss/.html/.js/.php e chama as tarefas respectivas para cada um deles.       
 **.scss**: 'sass' e browserSync.reload    
 **.html**: 'htmlupd' e browserSync.reload    
 **.js**: browserSync.reload    
 **.php**: browserSync.reload
* **useref**: Quando rodado, minifica o CSS da pasta src e coloca o resultado na pasta dist, tem também a opção para fazer o mesmo com o JS, está comentada por ser desnecessária pro projeto.
* **htmlupd**: [Requer 'htmldel'], pega o HTML da src (que utiliza os includes e sintaxe do Twig) e compila/minifica para a pasta dist.
* **htmldel**: Deleta o html da pasta dist para ser reposto novamente.
* **browserSync**: Inicia o [browserSync](https://www.browsersync.io/).
* **fonts**: Faz uma cópia de src/fonts para dist/fonts.
* **images**: Faz uma cópia de src/images para dist/images.
* **clean:dist**: Exclui a pasta dist para ser reposta.
* **default**: Tarefa que é iniciada junto com o Gulp. Roda em sequência a tarefa useref e a tarefa watch.

---
### Utilização:
O blocos do tipo **header** deve ser o primeiro a ser escolhido pelo usuário, pois este não pode ser arrastado.    
Depois do header, qualquer outro tipo de bloco pode ser escolhido e rearranjado, exceto os do tipo **footer**, que devem ser a última escolha.
---
### Funcionamento:
A parte de criação do template a partir dos blocos (step-4), utiliza a library [jQuery UI](https://jqueryui.com/) para o drag and drop.       

Cada bloco do sistema, que está listado na parte inferior da tela, possui um tipo e um ID.

Ao arrastar um bloco da lista e soltar no conteúdo (.sortable), um request é enviado para o arquivo get_blocos.php que verifica o ID enviado e busca um arquivo de bloco com ele. O retorno é o html do bloco que será colocado no sortable.