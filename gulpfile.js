/// Requires
var gulp          = require('gulp');
var sass          = require('gulp-sass');
var browserSync   = require('browser-sync').create();
var useref        = require('gulp-useref');
var uglify        = require('gulp-uglify');
var gulpIf        = require('gulp-if');
var cssnano       = require('gulp-cssnano');
var htmlmin       = require('gulp-htmlmin');
var del           = require('del');
var runSequence   = require('run-sequence');
var twig          = require('gulp-twig');
var data          = require('gulp-data');

function swallowError (error) {
  console.error(error.toString())
  this.emit('end')
}

/// Sass task
gulp.task('sass', function(){
  return gulp.src('src/sass/*.scss')
    .pipe(sass().on('error', swallowError))
    .pipe(gulp.dest('src/stylesheets'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

/// Gulp Watch
gulp.task('watch', ['browserSync', 'sass', 'fonts', 'images', 'htmlupd'], function(){
  gulp.watch('src/sass/**/*.scss', ['sass', browserSync.reload]);
  gulp.watch('src/**/*.html', ['htmlupd', browserSync.reload]);
  gulp.watch('src/js/**/*.js', browserSync.reload);
  gulp.watch('src/js/**/*.php', browserSync.reload);
  gulp.watch('src/fonts/**/*', ['fonts']);
  gulp.watch('src/images/**/*', ['images']);
});

/// Minifica o css e o JS e os coloca na dist, unificando-os em um só
gulp.task('useref', ['sass'], function(){
  return gulp.src(['src/**/*.js', 'src/**/*.css'])
    .pipe(useref())
    ///.pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

/// Exclui o html da dist e monta novamente baseado no html da src
gulp.task('htmldel', function(){
  return del.sync('dist/**/*.html')
});
gulp.task('htmlupd', ['htmldel'], function(){
  return gulp.src('src/**/*.html')
    .pipe(twig())
    .pipe(gulpIf('*.html', htmlmin({collapseWhitespace: true})))
    .pipe(gulp.dest('dist'))
});

/// browserSync
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: ['dist', '.']
    },
  })
});

/// Copy fonts to dist
gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
});

/// Copy images to dist
gulp.task('images', function() {
  return gulp.src('src/images/**/*')
  .pipe(gulp.dest('dist/images'))
});

/// Limpa os arquivos gerados
gulp.task('clean:dist', function() {
  return del.sync('dist');
});

/// Roda quando o gulp é iniciado
gulp.task('default', function(callback){
  /// Certifica que primeiro irá limpar o dist, depois reconstruí-lo e depois assisstir mudanças
  runSequence('clean:dist',
    ['useref', 'watch'],
    callback
  );
});
